dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_link ".config/lazygit/config.yml" "config.yml"
}

dpm_package_deps() {
  dpm_define_brew_dep "lazygit"
  dpm_define_pacman_dep "lazygit"
}
