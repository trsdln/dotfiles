#!/usr/bin/env sh

dpm_package_links() {
  dpm_define_script "clipboard.sh"
  dpm_define_script "dotfiles-helpers.sh"
  dpm_define_script "openfile"
  dpm_define_script "notify.sh"
}

dpm_package_deps() {
  dpm_define_pacman_dep "xdotool"
  # TODO: dpm_define_git_make "dmenu"
}
