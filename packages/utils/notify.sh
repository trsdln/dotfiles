#!/bin/sh

tput bel

notification_hint="string:x-canonical-private-synchronous:dotfiles-notify"

message="${1}"

if command -v notify-send >/dev/null; then
  notify-send --hint "${notification_hint}" "Dotfiles Notify" "$message"
elif command -v osascript >/dev/null; then
  osascript -e '
  on run argv
    set notifMsg to item 1 of argv
    display notification notifMsg
  end run' "${message}"
else
  echo "${message}"
fi
