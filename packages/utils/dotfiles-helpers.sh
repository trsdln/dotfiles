#!/bin/sh

echoerr() { echo "$@" 1>&2; }

_monterey_focus_crutch() {
  [ "$MONTEREY_FOCUS_CRUTCH" != "1" ] && return
  osascript -e '
  delay 0.1
  tell application "System Events"
    tell process "choose"
      set frontmost to true
      tell window 1
        select
      end tell
    end tell
  end tell
  ' >/dev/null
}

_fuzzy_finder() {
  local prompt=$1
  if [ ! -z "${WAYLAND_DISPLAY+x}" ]; then
    wofi --dmenu \
      --insensitive \
      --matching=fuzzy \
      --prompt="$prompt"
  elif (command -v dmenu >/dev/null); then
    dmenu -i -p "$prompt>"
  elif (command -v choose >/dev/null); then
    (_monterey_focus_crutch &)
    choose
  else
    fzf --prompt="$prompt>"
  fi
}

_ui_type_text_darwin() {
  # simple helper based on Apple Script
  osascript -e '
    on run argv
      set textToType to item 1 of argv
      tell application "System Events"
        keystroke textToType
      end tell
    end run' "${1}"
}

_ui_type_text() {
  if [ ! -z "${WAYLAND_DISPLAY+x}" ]; then
    wtype -d 20 -
  elif [ ! -z "${DISPLAY+x}" ]; then
    xdotool type --clearmodifiers --file -
  elif command -v osascript >/dev/null; then
    read -r text
    _ui_type_text_darwin "${text}"
  else
    echoerr "Error: typing is not supported"
    return 1
  fi
}
