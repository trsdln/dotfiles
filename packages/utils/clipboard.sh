#!/bin/sh

mode=$1 # get/set
if [ ! -z "${WAYLAND_DISPLAY+x}" ]; then
  [ "$mode" = "get" ] && wl-paste || wl-copy
elif $(command -v xclip >/dev/null); then
  [ "$mode" = "get" ] && xclip -out -selection clipboard || xclip -selection clipboard
elif $(command -v pbpaste >/dev/null); then
  [ "$mode" = "get" ] && pbpaste || pbcopy
else
  [ "$mode" = "get" ] && termux-clipboard-get || termux-clipboard-set
fi
