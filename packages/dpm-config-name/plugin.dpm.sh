#!/usr/bin/env sh

dpm_platform_config_name() {
  dpm_compatible "darwin" && echo "darwin" && return
  dpm_compatible "linux" && echo "linux" && return
  dpm_compatible "android" && echo "android" && return
  return 1
}
