#!/usr/bin/env sh

PULL_LOG_PATH="$HOME/.local/pull-log"

dpm_pull_log_init() {
  if [ ! -d "${PULL_LOG_PATH}" ]; then
    mkdir -p "${PULL_LOG_PATH}"
    git -C "${PULL_LOG_PATH}" init --quiet
  fi
}

dpm_pull_log_commit() {
  git -C "${PULL_LOG_PATH}" add .
  if ! git -C "${PULL_LOG_PATH}" diff --cached --exit-code --quiet; then
    git -C "${PULL_LOG_PATH}" commit \
      --quiet --no-gpg-sign \
      -m "${DPM_MODE} for ${git_repo_url}"
  fi
}

dpm_pull_log_git() {
  dpm_pass_error_exit || return
  [ "$DPM_ENABLE_PULL_LOG" != "1" ] && return

  dpm_echo_step "pull log" "${git_repo_url}"

  dpm_pull_log_init

  local pull_log_file="${PULL_LOG_PATH}/git.tsv"
  local tmp_log="${PULL_LOG_PATH}/git.tmp.tsv"

  # for some reason specifying "\t" directly
  # doesn't work on linux
  local tab_char=$(echo "\t")
  local local_path_exp="^${local_path}${tab_char}"

  if [ "${DPM_MODE}" = "remove" ]; then
    grep --invert-match "${local_path_exp}" \
      "${pull_log_file}" > ${tmp_log}
    rm ${pull_log_file}
    mv ${tmp_log} ${pull_log_file}
  else
    local latest_sha=$(git ${git_path_args} rev-parse --verify HEAD)
    local new_record="${local_path}\t${latest_sha}"

    local existing_sha
    existing_sha=$(grep "${local_path_exp}" "${pull_log_file}" 2>/dev/null)
    if [ "$?" = "0" ]; then
      existing_sha=${existing_sha##*	}
      if [ "${existing_sha}" = "${latest_sha}" ]; then 
        dpm_echo_step_status "Skipped"
        return
      fi 

      # replace old line with new one
      sed -E "s#${local_path_exp}.+#${new_record}#" \
        "${pull_log_file}" > "${tmp_log}"
      rm ${pull_log_file}
      mv ${tmp_log} ${pull_log_file}
    else
      echo "${new_record}" >>"${pull_log_file}"
    fi
  fi

  dpm_pull_log_commit
  dpm_echo_step_status
}

dpm_define_git_dep_after_hook() {
  dpm_pull_log_git
}
