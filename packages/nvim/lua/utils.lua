local function keymap_opts_factory(desc_prefix, bufnr)
  return function(desc)
    local opts = { noremap = true, silent = true }

    if bufnr ~= nil then
      opts.buffer = bufnr
    end

    if desc ~= nil then
      opts.desc = desc_prefix .. desc
    end

    return opts
  end
end

return { keymap_opts_factory = keymap_opts_factory }
