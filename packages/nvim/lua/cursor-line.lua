vim.wo.cursorline = true
local cursor_line_autocmd_group = vim.api.nvim_create_augroup("CursorLineGroup", { clear = true })
vim.api.nvim_create_autocmd("WinEnter", {
  group = cursor_line_autocmd_group,
  callback = function()
    vim.wo.cursorline = true
  end,
})
vim.api.nvim_create_autocmd("WinLeave", {
  group = cursor_line_autocmd_group,
  callback = function()
    vim.wo.cursorline = false
  end,
})
