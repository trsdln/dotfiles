local M = {}

function M.setup(from, to)
  vim.cmd([[
  cnoreabbrev <expr> ]] .. from .. [[
  ((getcmdtype() is# ":" && getcmdline() is# "]] .. from .. [[")
  \ ? ("]] .. to .. [[") : ("]] .. from .. [["))]])
end

return M
