local function config_harpoon()
  local harpoon = require("harpoon")
  local utils = require('../utils')
  local keymap_opts = utils.keymap_opts_factory('Harpoon: ')

  harpoon:setup({
    settings = {
      save_on_toggle = true,
    },
    default = {
      display = function(item)
        -- show row number at list item
        return string.format('%s:%d',
          item.value,
          item.context.row)
      end,
    }
  })

  local function get_git_branch()
    local git_branch = vim.fn.systemlist("git branch --show-current")
    if git_branch[1] == "fatal: not a git repository (or any of the parent directories): .git" then
      return nil
    end
    return git_branch[1]
  end

  local function get_current_list()
    local branch = get_git_branch()
    return harpoon:list(branch)
  end

  vim.keymap.set("n", "<leader>hh", function() get_current_list():add() end, keymap_opts('Add'))
  vim.keymap.set("n", "<leader>hw", function() harpoon.ui:toggle_quick_menu(get_current_list()) end,
    keymap_opts('Open Window'))

  vim.keymap.set("n", "<leader>hf", function() get_current_list():select(1) end, keymap_opts('First'))
  vim.keymap.set("n", "<leader>hd", function() get_current_list():select(2) end, keymap_opts('Second'))
  vim.keymap.set("n", "<leader>hs", function() get_current_list():select(3) end, keymap_opts('Third'))
  vim.keymap.set("n", "<leader>ha", function() get_current_list():select(4) end, keymap_opts('Fifth'))

  -- Toggle previous & next buffers stored within Harpoon list
  vim.keymap.set("n", "<leader>hr", function() get_current_list():prev() end, keymap_opts('Previous'))
  vim.keymap.set("n", "<leader>ht", function() get_current_list():next() end, keymap_opts('Next'))

  harpoon:extend({
    UI_CREATE = function(cx)
      local window_map_otps = utils.keymap_opts_factory('Harpoon Window: ', cx.bufnr)

      vim.keymap.set("n", "<C-v>", function()
        harpoon.ui:select_menu_item({ vsplit = true })
      end, window_map_otps('Vertical Split'))

      vim.keymap.set("n", "<C-x>", function()
        harpoon.ui:select_menu_item({ split = true })
      end, window_map_otps('Horizontal Split'))

      vim.keymap.set("n", "<C-t>", function()
        harpoon.ui:select_menu_item({ tabedit = true })
      end, window_map_otps('New Tab'))
    end,
  })
end

return {
  {
    'ThePrimeagen/harpoon',
    config = config_harpoon,
    branch = 'harpoon2',
    dependencies = { 'nvim-lua/plenary.nvim' },
  }
}
