local file_exists = function(name)
  local f = io.open(name, "r")
  if f ~= nil then
    io.close(f)
    return true
  else
    return false
  end
end

return {
  {
    'mfussenegger/nvim-lint',
    config = function()
      vim.env.ESLINT_D_PPID = vim.fn.getpid()
      local js_linter = 'eslint_d'
      if file_exists('./eslint.config.mjs') then
        js_linter = 'eslint'

        -- resolve it while using PNP
        require('lint').linters.eslint.cmd = 'yarn'
        require('lint').linters.eslint.args = {
          'eslint',
          '--format',
          'json',
          '--stdin',
          '--stdin-filename',
          function() return vim.api.nvim_buf_get_name(0) end,
        }
      end

      require('lint').linters_by_ft = {
        javascript = { js_linter },
        typescript = { js_linter },
        typescriptreact = { js_linter },
      }

      local linter_group = vim.api.nvim_create_augroup("NvimLintGroup", { clear = true })
      vim.api.nvim_create_autocmd({ "BufReadPre", "InsertLeavePre", "BufWritePost" }, {
        pattern = { "*.js", "*.cjs", "*.jsx", "*.mjs", "*.ts", "*tsx" },
        group = linter_group,
        callback = function()
          -- for the current filetype
          require("lint").try_lint()
        end,
      })
    end,
  }
}
