local function get_table_keys(t)
  local keys = {}
  for key, _ in pairs(t) do
    table.insert(keys, key)
  end
  return keys
end

local function xdg_open(target)
  local os_name = vim.loop.os_uname().sysname
  local open_cmd
  if os_name == 'Darwin' then
    open_cmd = 'open'
  else
    open_cmd = 'xdg-open'
  end
  os.execute(open_cmd .. ' "' .. target .. '"')
end

local function create_telescope_dropdown()
  local pickers = require "telescope.pickers"
  local finders = require "telescope.finders"
  local conf = require("telescope.config").values
  local actions = require "telescope.actions"
  local action_state = require "telescope.actions.state"
  local telescope_options = require("telescope.themes").get_dropdown {}

  return function(options, on_select)
    pickers.new(telescope_options, {
      prompt_title = "library",
      finder = finders.new_table {
        results = options
      },
      sorter = conf.generic_sorter(telescope_options),
      attach_mappings = function(prompt_bufnr)
        actions.select_default:replace(function()
          actions.close(prompt_bufnr)
          local selection = action_state.get_selected_entry()
          on_select(selection[1])
        end)
        return true
      end,
    }):find()
  end
end

local create_open_docs_mapping = function()
  local docs_sources_map = {
    ['date-fns'] = 'https://date-fns.org/v2.21.1/docs/',
    ['fluture'] = 'https://github.com/fluture-js/Fluture#',
    ['mongodb'] = 'https://mongodb.github.io/node-mongodb-native/5.1/classes/Collection.html#',
    ['ramda'] = 'https://ramdajs.com/docs/#',
    ['recompose'] = 'https://github.com/acdlite/recompose/blob/master/docs/API.md#',
    ['sanctuary'] = 'https://sanctuary.js.org/#',
    ['styled-componets'] = 'https://www.styled-components.com/docs/api#'
  }

  local telescope_dropdown = create_telescope_dropdown()

  return function()
    telescope_dropdown(get_table_keys(docs_sources_map), function(selected_lib)
      local base_url = docs_sources_map[selected_lib]
      local current_word = vim.fn.expand('<cword>')
      xdg_open(base_url .. current_word)
    end)
  end
end

return {
  {
    'nvim-telescope/telescope.nvim',
    config = function()
      local telescope = require('telescope')
      local actions = require("telescope.actions")

      telescope.setup({
        defaults = {
          path_display = { truncate = 1 },
          layout_strategy = 'vertical',
          layout_config = { height = 0.95, width = 0.95 },
          mappings = {
            i = {
              ['<C-j>'] = actions.move_selection_next,
              ['<C-k>'] = actions.move_selection_previous,
              ['<C-l>'] = actions.send_selected_to_qflist + actions.open_qflist,
            },
            n = {
              ['<C-j>'] = actions.move_selection_next,
              ['<C-k>'] = actions.move_selection_previous,
              ['<C-l>'] = actions.send_selected_to_qflist + actions.open_qflist,
            }
          }
        }
      })

      -- Enable telescope extensions, if they are installed
      pcall(require('telescope').load_extension, 'fzf')
      pcall(require('telescope').load_extension, 'ui-select')

      local builtin = require('telescope.builtin')
      local utils = require('../utils')
      local keymap_opts = utils.keymap_opts_factory('Telescope: ')

      vim.keymap.set('n', '<C-p>', builtin.find_files, keymap_opts('Files'))
      vim.keymap.set('n', '<leader>ft', builtin.tags, keymap_opts('Tags'))
      vim.keymap.set('n', '<leader>fk', builtin.keymaps, keymap_opts('Key Maps'))

      vim.o.tags = vim.o.tags .. ';,.tags'
      vim.keymap.set('n', '<leader>fh', builtin.help_tags, keymap_opts('Help Tags'))

      vim.keymap.set('n', '<leader>fr', builtin.resume, keymap_opts('Resume'))
      vim.keymap.set('n', '<leader>fg', builtin.live_grep, keymap_opts('Live Grep'))

      -- todo: limit it to javascript file type
      local open_docs = create_open_docs_mapping()
      vim.keymap.set('n', '<leader>fd', open_docs, keymap_opts('Open Library Docs'))
    end,
    dependencies = {
      'nvim-lua/plenary.nvim',
      {
        'nvim-telescope/telescope-fzf-native.nvim',
        build = 'make',
        cond = function()
          return vim.fn.executable 'make' == 1
        end,
      },
      'nvim-telescope/telescope-ui-select.nvim',
    },
  },
}
