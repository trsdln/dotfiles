local setup_completion = function()
  local cmp = require('cmp')

  local opened_buffers_source = {
    name = 'buffer',
    option = {
      get_bufnrs = function()
        return vim.api.nvim_list_bufs()
      end
    }
  }

  cmp.setup({
    completion = {
      autocomplete = false
    },
    snippet = {
      expand = function(args)
        require('snippy').expand_snippet(args.body)
      end,
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-y>'] = cmp.mapping.confirm({ select = true }),
      ['<CR>'] = cmp.mapping.confirm({ select = true }),
      ['<C-Space>'] = cmp.mapping.complete({}),
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-e>'] = cmp.mapping.abort(),
    }),
    sources = cmp.config.sources({
      { name = 'snippy' },
      { name = 'lazydev', group_index = 0 },
      { name = 'nvim_lsp' },
      { name = 'nvim_lua' },
      {
        name = 'tags',
        keyword_length = 5,
        option = {
          max_items = 5,
          exact_match = true,
          current_buffer_only = false,
        },
      },
      opened_buffers_source,
    })
  })

  cmp.setup.filetype({ 'sh', 'yaml' }, {
    sources = cmp.config.sources({
      { name = 'path' },
      opened_buffers_source,
    })
  })
end

return {
  {
    'hrsh7th/nvim-cmp',
    event = 'InsertEnter',
    config = setup_completion,
    dependencies = {
      'hrsh7th/cmp-buffer',
      'hrsh7th/cmp-path',
      'hrsh7th/cmp-nvim-lua',
      'hrsh7th/cmp-nvim-lsp',
      'dcampos/cmp-snippy',
      { 'quangnguyen30192/cmp-nvim-tags', ft = { 'javascript' } },
    },
  },
}
