return {
  {
    'ishan9299/nvim-solarized-lua',
    lazy = false,
    priority = 1000,
    init = function()
      vim.g.solarized_italics = 1
      vim.g.solarized_statusline = 'flat'
      vim.g.solarized_visibility = 'normal'
      vim.g.solarized_diffmode = 'normal'
      vim.g.solarized_termtrans = 1
      vim.o.termguicolors = true
      vim.cmd.colorscheme('solarized')

      -- due to solarized_termtrans option this one is disabled:
      vim.cmd([[hi CursorLine guibg=#073642 ctermbg=23]])
    end,
  }
}
