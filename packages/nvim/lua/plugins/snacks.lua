return {
  {
    'folke/snacks.nvim',
    priority = 1000,
    lazy = false,
    opts = {
      bigfile = {
        -- use default settings
      },
      scroll = {
        -- use default settings
      },
      lazygit = {
        config = {
          gui = {
            nerdFontsVersion = "", -- disable icons
          },
        }
      }
    },
    keys = {
      { '<leader>gl', function() require('snacks').lazygit.open() end, desc = 'Snacks: Open Lazy Git' },
    }
  }
}
