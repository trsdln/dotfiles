return {
  {
    'kristijanhusak/vim-js-file-import',
    ft = { 'javascript' },
    init = function()
      vim.g.js_file_import_strip_file_extension = 0
      vim.g.js_file_import_strip_index_file = 0
      vim.g.js_file_import_sort_after_insert = 0 -- is buggy for multi line imports
      vim.g.js_file_import_use_fzf = 1
    end,
    build = 'npm install',
    dependencies = {
      {
        'ludovicchabant/vim-gutentags',
        init = function()
          vim.cmd [[
            let g:gutentags_modules = ['ctags']

            " Index only files included into VCS repo
            let g:gutentags_file_list_command = {
                  \   'markers': {
                  \     '.git': 'git ls-files',
                  \   },
                  \ }
            let g:gutentags_resolve_symlinks = 0
            let g:gutentags_ctags_tagfile = '.tags'
            let g:gutentags_ctags_exclude = [
                  \ '*.yaml', '*.yml', '*.sh', '*.md', '*.lock', '*.css', '*.json',
                  \ ".yarn/*",
                  \ "node_modules/*",
                  \ "packages/poly-react-scripts/*",
                  \ "schema.json",
                  \ "docs/*",
                  \ ".dump/*",
                  \ ".db/*"
                  \ ]
            let g:gutentags_ctags_exclude_wildignore = 1

            " Disable methods and object properties
            let g:gutentags_ctags_extra_args = ['--javascript-kinds=-m-p', '--map-JavaScript=+.cjs']
            ]]

          vim.api.nvim_create_autocmd('User', {
            pattern = { "GutentagsUpdating", "GutentagsUpdated" },
            group = vim.api.nvim_create_augroup("GutentagsStatusline", { clear = true }),
            callback = function()
              require('lualine').refresh()
            end,
          })
        end,
      },
      {
        'junegunn/fzf.vim',
        dependencies = { 'junegunn/fzf' },
        init = function()
          vim.cmd [[
        let g:fzf_layout = { 'down': '~40%' }

        let g:fzf_colors =
        \ { 'fg':      ['fg', 'Normal'],
        \ 'bg':      ['bg', 'Normal'],
        \ 'hl':      ['fg', 'Comment'],
        \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
        \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
        \ 'hl+':     ['fg', 'Statement'],
        \ 'info':    ['fg', 'PreProc'],
        \ 'border':  ['fg', 'Ignore'],
        \ 'prompt':  ['fg', 'Conditional'],
        \ 'pointer': ['fg', 'Exception'],
        \ 'marker':  ['fg', 'Keyword'],
        \ 'spinner': ['fg', 'Label'],
        \ 'header':  ['fg', 'Comment'] }
        ]]
        end
      },
    }
  },
}
