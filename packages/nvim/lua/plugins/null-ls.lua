return {
  {
    -- Potentially maintained version: nvimtools/none-ls.nvim
    'jose-elias-alvarez/null-ls.nvim',
    enabled = false,
    ft = { 'javascript' },
    config = function()
      local lsp_common = require('../lsp-common')
      local null_ls = require('null-ls')

      local project_root_cwd_opts = {
        -- Too smart handling for this causes
        -- unexpected behavior inside monorepo
        cwd = function()
          return nil
        end,
      }

      null_ls.setup({
        sources = {
          null_ls.builtins.diagnostics.eslint_d.with(project_root_cwd_opts),
          null_ls.builtins.code_actions.eslint_d.with(project_root_cwd_opts),
        },
        on_attach = lsp_common.on_attach,
      })
    end,
    dependencies = { 'nvim-lua/plenary.nvim' },
  }
}
