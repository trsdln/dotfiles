return {
  {
    'mg979/vim-visual-multi',
    init = function()
      -- Note: the plugin is not compatible with lua tables
      vim.cmd [[
        let g:VM_maps                       = {}
        let g:VM_maps['Add Cursor Down'] = '<M-Down>'
        let g:VM_maps['Add Cursor Up']   = '<M-Up>'
        let g:VM_set_statusline = 0 " prevent statusline blinking
      ]]
    end
  }
}
