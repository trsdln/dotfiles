return {
  {
    'NvChad/nvim-colorizer.lua',
    ft = { 'css', 'javascript', 'html' },
    opts = {
      filetypes = {
        'css',
        'javascript',
        html = { mode = 'foreground', }
      },
      user_default_options = {
        rgb_fn = true, -- CSS rgb() and rgba() functions
        mode = 'virtualtext',
      },
    }
  }
}
