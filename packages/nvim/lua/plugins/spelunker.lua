return {
  {
    'kamykn/spelunker.vim',
    init = function()
      vim.g.spelunker_disable_auto_group = 1

      vim.api.nvim_create_autocmd({ 'BufWinEnter', 'BufWritePost' }, {
        group = vim.api.nvim_create_augroup("SpelunkerCheckFile", {
          clear = true
        }),
        pattern = {
          '*.vim', '*.js', '*.cjs', '*.jsx', '*.json',
          '*.sh', '*.md', '*.rs', '*.lua', '*.zig',
          '*.ts', '*.tsx',
          'COMMIT_EDITMSG'
        },
        callback = function()
          local target_file = vim.fn.expand('%:t')
          if target_file == 'schema.json'
              or target_file == 'yarn.lock'
          then
            return
          end

          vim.api.nvim_call_function('spelunker#check', {})
        end,
      })
    end,
  }
}
