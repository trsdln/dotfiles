return {
  {
    'nvim-lualine/lualine.nvim',
    config = function()
      local mode_map = {
        ['NORMAL'] = 'N',
        ['VISUAL'] = 'V',
        ['REPLACE'] = 'R',
        ['V-LINE'] = 'VL',
        ['V-BLOCK'] = 'VB',
        ['INSERT'] = 'I',
        ['COMMAND'] = 'C',
      }

      local gutentags_status = function()
        return vim.api.nvim_call_function('gutentags#statusline', {})
      end

      require('lualine').setup({
        options = {
          theme = 'solarized_dark',
          component_separators = { left = '', right = '' },
          section_separators = { left = '', right = '' },
        },
        sections = {
          lualine_a = {
            {
              'mode',
              fmt = function(str)
                return mode_map[str] or str
              end
            },
          },
          lualine_b = {
            {
              'filename',
              symbols = {
                modified = '+',
                readonly = '-',
                unnamed = '[No Name]',
                newfile = '[New]',
              }
            }
          },
          lualine_c = {},
          lualine_x = { gutentags_status },
          lualine_y = { 'progress' },
          lualine_z = { 'location' }
        },
        inactive_sections = {
          lualine_a = {},
          lualine_b = {},
          lualine_c = { 'filename' },
          lualine_x = { 'location' },
          lualine_y = {},
          lualine_z = {}
        },
        tabline = {
          lualine_a = {
            {
              'tabs',
              max_length = function()
                return vim.o.columns
              end,
              mode = 1,
            },
          },
        },
        extensions = { 'fugitive', 'fzf' },
      })

      -- Override setting after lualine loads:
      vim.defer_fn(function()
        -- Hide annoying tab line if tabs aren't used
        vim.o.showtabline = 1
        vim.o.showmode = false
      end, 0)
    end
  }
}
