local lsp_common = require('../lsp-common')

local setup_lsp = function()
  local utils = require('../utils')

  local rt = require('rust-tools')
  rt.setup({
    tools = {
      hover_actions = {
        border = 'none',
      },
    },
    server = {
      on_attach = function(client, bufnr)
        lsp_common.on_attach(client, bufnr)

        local keymap_opts = utils.keymap_opts_factory('LSP: ', bufnr)

        vim.keymap.set('n', '<leader>lh', rt.hover_actions.hover_actions,
          keymap_opts('Hover Actions'))
        vim.keymap.set('n', '<leader>lg', rt.code_action_group.code_action_group,
          keymap_opts('Code Action Group'))
        vim.keymap.set('n', '<leader>R', rt.runnables.runnables,
          keymap_opts('Rust: Runnables'))
        vim.keymap.set('n', '<leader>lbd',
          require("rust-tools.external_docs").open_external_docs,
          keymap_opts('Rust: Browse Docs'))
      end,
      settings = {
        ["rust-analyzer"] = {
          lens = {
            enable = true,
          },
          checkOnSave = {
            enable = true,
            command = "clippy",
          },
        },
      },
    },
  })

  local lspconfig = require('lspconfig')
  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities = vim.tbl_deep_extend('force', capabilities, require('cmp_nvim_lsp').default_capabilities())

  lspconfig.lua_ls.setup {
    capabilities = capabilities,
    on_attach = lsp_common.on_attach,
    flags = {
      debounce_text_changes = 150,
    },
    settings = {
      Lua = {
        runtime = {
          version = 'LuaJIT',
        },
        workspace = {
          checkThirdParty = false,
          library = {
            '${3rd}/luv/library',
            unpack(vim.api.nvim_get_runtime_file('', true)),
          },
        },
        telemetry = {
          enable = false,
        },
      },
    },
  }

  lspconfig.gopls.setup({
    on_attach = lsp_common.on_attach,
    settings = {
      gopls = {
        analyses = {
          unusedparams = true,
        },
        staticcheck = true,
        gofumpt = true,
      },
    },
  })
  vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = "*.go",
    group = vim.api.nvim_create_augroup("GoPlsOrganizeImports", { clear = true }),
    callback = function()
      local params = vim.lsp.util.make_range_params()
      params.context = { only = { "source.organizeImports" } }
      -- buf_request_sync defaults to a 1000ms timeout. Depending on your
      -- machine and codebase, you may want longer. Add an additional
      -- argument after params if you find that you have to write the file
      -- twice for changes to be saved.
      -- E.g., vim.lsp.buf_request_sync(0, "textDocument/codeAction", params, 3000)
      local result = vim.lsp.buf_request_sync(0, "textDocument/codeAction", params)
      for cid, res in pairs(result or {}) do
        for _, r in pairs(res.result or {}) do
          if r.edit then
            local enc = (vim.lsp.get_client_by_id(cid) or {}).offset_encoding or "utf-16"
            vim.lsp.util.apply_workspace_edit(r.edit, enc)
          end
        end
      end
      vim.lsp.buf.format({ async = false })
    end
  })

  -- -- vim.lsp.set_log_level("debug")
  -- lspconfig.ts_ls.setup({
  --   on_attach = lsp_common.on_attach,
  --   init_options = {
  --     tsserver = {
  --       -- Note: can be used for debugging tsserver related issues:
  --       -- logVerbosity = 'normal',
  --     },
  --     preferences = {
  --       disableSuggestions = true,
  --       quotePreference = 'single'
  --     }
  --   },
  --   filetypes = {
  --     "typescript",
  --     "typescriptreact",
  --     "javascript",
  --   },
  -- })

  require('lspconfig').zls.setup({
    on_attach = lsp_common.on_attach,
  })
end

local setup_typescript_tools = function()
  local on_attach = function(_, bufnr)
    lsp_common.on_attach(_, bufnr)
    vim.keymap.set('n', '<leader>li', "<cmd>TSToolsAddMissingImports<cr>",
      {
        noremap = true,
        silent = true,
        buffer = bufnr,
        desc = 'LSP: Add missing imports'
      })
  end

  require('typescript-tools').setup({
    on_attach = on_attach,
    settings = {
      tsserver_file_preferences = {
        quotePreference = "single",
      },
    }
  })

  vim.api.nvim_create_autocmd("BufWritePre", {
    pattern = { "*.ts", "*.tsx" },
    group = vim.api.nvim_create_augroup("TypescriptOrganizeImports", { clear = true }),
    callback = function()
      require("typescript-tools.api").organize_imports(true)
    end
  })
end

return {
  {
    'folke/lazydev.nvim',
    ft = 'lua',
    opts = {
      library = {
        -- Load luvit types when the `vim.uv` word is found
        { path = '${3rd}/luv/library', words = { 'vim%.uv' } },
      },
    },
  },
  {
    'neovim/nvim-lspconfig',
    config = setup_lsp,
    dependencies = {
      'simrat39/rust-tools.nvim',
      'hrsh7th/cmp-nvim-lsp',
    }
  },
  {
    "pmizio/typescript-tools.nvim",
    ft = { 'javascript', 'javascript' },
    dependencies = { "nvim-lua/plenary.nvim", "neovim/nvim-lspconfig" },
    config = setup_typescript_tools,
  },
  {
    url = 'https://gitlab.com/schrieveslaach/sonarlint.nvim.git',
    ft = { 'javascript', 'typescript' },
    enabled = false,
    priority = 1, -- load after lspconfig
    opts = {
      server = {
        cmd = {
          'sonarlint-language-server',
          -- Ensure that sonarlint-language-server uses stdio channel
          '-stdio',
          '-analyzers',
          -- paths to the analyzers that needed
          vim.fn.expand("$MASON/share/sonarlint-analyzers/sonarjs.jar"),
        },
        settings = {
          sonarlint = {
            rules = {
              ["typescript:S101"] = { level = "on", parameters = { format = "^[A-Z][a-zA-Z0-9]*$" } },
              ["typescript:S103"] = { level = "on", parameters = { maximumLineLength = 180 } },
              ["typescript:S106"] = { level = "on" },
              ["typescript:S107"] = { level = "on", parameters = { maximumFunctionParameters = 7 } },
            },
          },
        },
      },
      filetypes = {
        'typescript',
        'javascript',
      },
    },
    dependencies = {
      {
        'williamboman/mason.nvim',
        opts = {},
      },
      {
        'WhoIsSethDaniel/mason-tool-installer.nvim',
        opts = {
          ensure_installed = {
            'sonarlint-language-server'
          },
          integrations = {
            ['mason-lspconfig'] = false,
            ['mason-null-ls'] = false,
            ['mason-nvim-dap'] = false,
          },
        },
      },
    },
  }
}
