local function setup_fugitive()
  local command_alias = require('../command-alias')

  command_alias.setup("gpu", "Git push --no-verify")
  command_alias.setup("gpl", "Git pull")
  command_alias.setup("g", "Git")

  vim.keymap.set('n', '<leader>gs', ':Git<CR>', {
    noremap = true,
    desc = 'Git: status'
  })

  vim.cmd [[
    function! s:CodeReviewStart(...)
      let s:branch = get(a:, 1, 'origin/main')
      echo "Code review against " . s:branch

      tabnew
      execute 'Git! difftool --name-status ' . s:branch
      call s:CodeReviewChangeFile('cc')
    endfunction

    function! s:CodeReviewChangeFile(dir_command)
      try
        execute a:dir_command
        wincmd o
        " Gvdiffsplit treats renamed files as new files
        " therefore we should find out old file name
        " and do diff against it.
        let old_file_name = s:GitGetOldName(s:branch, expand('%'))
        silent execute 'Gvdiffsplit ' . s:branch . ':' . old_file_name
        wincmd r
        " Note: switching to right buffer (i.e. `wincmd w`)
        " causes random problem with diff
      catch
        let err_msg = substitute(v:exception, '\v^Vim\(\w+\):E\d+:\s', "", "")
        echo err_msg
      endtry
    endfunction

    function! s:GitGetOldName(git_rev, current_name)
      let diff_cmd = 'git diff --diff-filter=R ' . a:git_rev .'..@'
      let grep_cmd = 'grep -E ''^diff --git \S+\sb/'. a:current_name .'$'''
      let log_output = system(diff_cmd . ' | ' . grep_cmd)

      if log_output !=# ''
        let log_last_line = split(log_output, ' ')[-2]
        return log_last_line[2:]
      endif

      return a:current_name
    endfunction

    command! -nargs=? GitCodeReview :call s:CodeReviewStart(<f-args>)
    command! -nargs=1 GitCodeReviewChangeFile :call s:CodeReviewChangeFile(<f-args>)

    noremap <silent> [d :GitCodeReviewChangeFile cpfile<cr>
    noremap <silent> ]d :GitCodeReviewChangeFile cnfile<cr>
  ]]
end

return {
  {
    'tpope/vim-fugitive',
    init = setup_fugitive,
  },
  -- Github links
  'tpope/vim-rhubarb',
  -- Gitlab links
  'shumphrey/fugitive-gitlab.vim',
  -- Bitbucket links
  'tommcdo/vim-fubitive',
  -- Git log add-on for fugitive
  {
    'junegunn/gv.vim',
    cmd = { 'GV' },
  },
}
