return {
  {
    'justinmk/vim-dirvish',
    init = function()
      vim.g.dirvish_mode = ':sort ,^.*[\\/],'
      vim.g.loaded_netrwPlugin = 1

      local open_tree = function()
        vim.cmd.vsplit('.')
      end
      vim.keymap.set('n', '<leader>t', open_tree, {
        noremap = true,
        silent = true,
        desc = 'Dirvish: Open Root'
      })

      local reveal_file = function()
        vim.cmd.vsplit()
        vim.cmd.Dirvish(vim.fn.expand('%:p:h'))
      end
      vim.keymap.set('n', '<leader>r', reveal_file, {
        noremap = true,
        silent = true,
        desc = 'Dirvish: Open Current File Directory'
      })
    end,
  }
}
