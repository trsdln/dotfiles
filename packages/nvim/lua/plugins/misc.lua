return {
  {
    'kovetskiy/sxhkd-vim',
    ft = 'sxhkd'
  },
  {
    'vim-scripts/workflowish',
    ft = 'workflowish'
  },
  'nelstrom/vim-visual-star-search',
}
