local function setup_eunuch()
  vim.cmd [[
    " Triggers ctags reindex for empty file before removal
    " so obsolete definitions are removed from ctags
    command! RemoveJS execute 'normal!ggdG' | write! | Delete
    command! DeleteJS RemoveJS

    " Restore rename's relative path behavior
    command! -nargs=1 Rename :Move %:h/<args>

    " Keeping this at ftplugin for JS specifically causes errors
    function! s:RenameJS(newName)
      execute 'normal!ggdG'
      write!
      execute 'Rename ' . a:newName
      execute 'normal!u'
      write!
    endfunction

    command! -nargs=1 RenameJS call s:RenameJS(<f-args>)

    function! s:MoveJS(newName)
      execute 'normal!ggdG'
      write!
      execute 'Move ' . a:newName
      execute 'normal!u'
      write!
    endfunction

    command! -nargs=1 MoveJS call s:MoveJS(<f-args>)
 ]]
end

local function setup_obsession()
  vim.cmd [[
    set sessionoptions=curdir,winpos,buffers,winsize,help,tabpages

    function! s:ToggleObsession()
      if ObsessionStatus() ==# '[$]'
        Obsession
      else
        Obsession Session.vim
      endif
    endfunction

    command! ToggleObsession :call s:ToggleObsession()

    " Start/Stop session tracking
    nnoremap <leader>ss :ToggleObsession<CR>

    " Load default session
    nnoremap <leader>so :source Session.vim<CR>
 ]]
end

local function setup_dispatch()
  vim.cmd [[
    let g:dispatch_tmux_height = 0
 ]]
end

return {
  {
    'tpope/vim-surround',
    dependencies = {
      'tpope/vim-repeat',
    }
  },
  {
    'tpope/vim-eunuch',
    config = setup_eunuch
  },
  'tpope/vim-abolish',
  {
    'tpope/vim-obsession',
    init = setup_obsession
  },
  {
    'tpope/vim-dispatch',
    init = setup_dispatch
  },
  {
    'tpope/vim-dadbod',
    cmd = { 'DB' },
    ft = { 'javascript', 'sql' },
  },
}
