return {
  'stevearc/conform.nvim',
  config = function()
    local prefer_prettierd = { "prettierd", "prettier", stop_after_first = true }
    require("conform").setup({
      notify_on_error = false,
      formatters_by_ft = {
        -- Use a sub-list to run only the first available formatter
        javascript = prefer_prettierd,
        typescript = prefer_prettierd,
        typescriptreact = prefer_prettierd,
        yaml = prefer_prettierd,
        markdown = prefer_prettierd,
        html = prefer_prettierd,
        css = prefer_prettierd,
        json = prefer_prettierd,
      },
      formatters = {
        prettierd = {
          -- Fix prettierd CWD detection:
          require_cwd = true,
          cwd = require('conform.util').root_file({
            '.prettierignore',
            '.prettierrc',
            'prettier.config.js',
            'prettier.config.cjs',
            'prettier.config.mjs',
          }),
        },
      },
    })

    vim.api.nvim_create_autocmd('BufWritePre', {
      pattern = '*',
      group = vim.api.nvim_create_augroup('ConformFormatGroup', { clear = true }),
      callback = function(args)
        -- we use GoPlsOrganizeImports group for Go
        if vim.bo.filetype ~= 'go' then
          require('conform').format({
            bufnr = args.buf,
            lsp_format = 'fallback',
          })
        end
      end,
    })
  end,
};
