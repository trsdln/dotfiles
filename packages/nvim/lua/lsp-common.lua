local utils = require('./utils')
local diag_keymap_opts = utils.keymap_opts_factory('Diagnostic: ')

-- diagnostic mappings
vim.keymap.set('n', '[e', vim.diagnostic.goto_prev, diag_keymap_opts('Previous'))
vim.keymap.set('n', ']e', vim.diagnostic.goto_next, diag_keymap_opts('Next'))
vim.keymap.set('n', '<leader>df', vim.diagnostic.open_float, diag_keymap_opts('Open Float'))
vim.keymap.set('n', '<leader>dl', vim.diagnostic.setloclist, diag_keymap_opts('To Location List'))

local M = {}

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
M.on_attach = function(_, bufnr)
  -- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_set_option_value('omnifunc', 'v:lua.vim.lsp.omnifunc', { buf = bufnr })

  local lsp_keymap_opts = utils.keymap_opts_factory('LSP: ', bufnr);
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, lsp_keymap_opts('Go To Definition'))
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, lsp_keymap_opts('Show References'))
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, lsp_keymap_opts('Symbol Hover Info'))
  vim.keymap.set('n', '<leader>lr', vim.lsp.buf.rename, lsp_keymap_opts('Rename'))
  vim.keymap.set('n', '<leader>la', vim.lsp.buf.code_action, lsp_keymap_opts('Code Action'))
  vim.keymap.set('i', '<C-a>', vim.lsp.buf.signature_help, lsp_keymap_opts('Signature Help'))
end

return M
