local utils = require('./utils')

vim.keymap.set('n', '<C-h>', '<C-w><C-h>', { desc = 'Move focus to the left window' })
vim.keymap.set('n', '<C-l>', '<C-w><C-l>', { desc = 'Move focus to the right window' })
vim.keymap.set('n', '<C-j>', '<C-w><C-j>', { desc = 'Move focus to the lower window' })
vim.keymap.set('n', '<C-k>', '<C-w><C-k>', { desc = 'Move focus to the upper window' })

vim.keymap.set('n',
  '<leader>ba',
  '<cmd>bufdo bd<cr>',
  { noremap = true, desc = 'Close all buffers' })

-- Set highlight on search, but clear on pressing <Esc> in normal mode
vim.go.hlsearch = true
vim.keymap.set('n', '<Esc>', '<cmd>nohlsearch<CR>')
vim.keymap.set('n', '<leader>n', function() print('Deprecated: use <ESC> instead.') end)

-- QuickFix file entries navigation
local quickfix_map_opts = utils.keymap_opts_factory('QuickFix: ')
vim.keymap.set('n', '[f', ':cpfile<CR>', quickfix_map_opts('Previous file'))
vim.keymap.set('n', ']f', ':cnfile<CR>', quickfix_map_opts('Next file'))
vim.keymap.set('n', '[q', ':cprevious<CR>', quickfix_map_opts('Previous entry'))
vim.keymap.set('n', ']q', ':cnext<CR>', quickfix_map_opts('Next entry'))

-- Resize buffer window: Alt+*
local resize_map_opts = utils.keymap_opts_factory('Resize Window: ')
vim.keymap.set('n', '<M-j>', ':resize +1<CR>', resize_map_opts('Increase Width'))
vim.keymap.set('n', '<M-k>', ':resize -1<CR>', resize_map_opts('Decrease Width'))
vim.keymap.set('n', '<M-l>', ':vertical resize +5<CR>', resize_map_opts('Increase Height'))
vim.keymap.set('n', '<M-h>', ':vertical resize -5<CR>', resize_map_opts('Decrease Height'))

local split_map_opts = utils.keymap_opts_factory('Split Last File: ')
vim.keymap.set('n', '<leader>vt', ':execute "rightbelow vsplit " . bufname("#")<CR>', split_map_opts('Vertical'))
vim.keymap.set('n', '<leader>hz', ':execute "rightbelow split " . bufname("#")<CR>', split_map_opts('Horizontal'))

-- Keep current line at center while scrolling
-- (Interferes with Snacks' scroll animation)
-- vim.keymap.set('n', '<C-u>', '<C-u>zz', { noremap = true })
-- vim.keymap.set('n', '<C-d>', '<C-d>zz', { noremap = true })
-- Put search match at the center of screen
vim.keymap.set('n', 'n', 'nzz', { noremap = true })
vim.keymap.set('n', 'N', 'Nzz', { noremap = true })

-- Use sane regexes
vim.keymap.set('n', '/', '/\\v', { noremap = true })
vim.keymap.set('n', '/', '/\\v', { noremap = true })
vim.keymap.set('n', '?', '?\\v', { noremap = true })
vim.keymap.set('n', '?', '?\\v', { noremap = true })

-- Fix repeat last substitute (to keep flags)
vim.keymap.set('n', '&', ':&&<CR>', { noremap = true, silent = true })
vim.keymap.set('n', '&', ':&&<CR>', { noremap = true, silent = true })

-- Add new line without entering insert mode
vim.keymap.set('n', 'oo', 'o<Esc>k', { noremap = true })
vim.keymap.set('n', 'OO', 'O<Esc>j', { noremap = true })

-- UPPERCASE_CONSTANTS_EASILY with Ctrl-U
vim.keymap.set('i', '<C-U>', '<Esc>viwUea', { noremap = true })
-- Capitalize Word with Ctrl-K
vim.keymap.set('i', '<C-K>', '<Esc>b~ea', { noremap = true })

vim.keymap.set('n', '<leader>w', ':silent write!<cr>', { noremap = true, silent = true })

vim.keymap.set('n', 'cp', '"+y', { noremap = true })
vim.keymap.set('v', 'cp', '"+y', { noremap = true })
vim.keymap.set('n', 'cP', '"+yy', { noremap = true })
vim.keymap.set('n', 'cv', '"+p', { noremap = true })
vim.keymap.set('n', 'cV', '"+P', { noremap = true })

vim.keymap.set('t', '<Esc>', '<C-\\><C-n>', { noremap = true })
vim.keymap.set('t', '<C-v><Esc>', '<Esc>', { noremap = true })
