vim.api.nvim_create_autocmd('BufReadPost', {
  group = vim.api.nvim_create_augroup('restore_pos_autocmd', {}),
  callback = function()
    local line = vim.fn.line("'\"")
    local is_valid = line > 1 and line <= vim.fn.line("$")
    if is_valid and vim.bo.filetype ~= 'commit' then
      vim.cmd [[normal! g`"]]
    end
  end,
})
