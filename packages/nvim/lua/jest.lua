local function jest_start_test_suite()
  -- remove extension, so we can match by both
  -- test file and file being tested as long as
  -- names match
  local file_name = vim.fn.expand('%'):gsub('.js$', ''):gsub('.ts$', '')
  local vim_cmd = 'vsplit | terminal yarn jest --watch ' .. file_name
  vim.cmd [[silent write!]]
  vim.cmd(vim_cmd)
end

vim.api.nvim_create_autocmd('FileType', {
  pattern = { 'javascript', 'typescript' },
  group = vim.api.nvim_create_augroup('JestConfig', { clear = true }),
  callback = function(args)
    vim.keymap.set('n', '<leader>jv', jest_start_test_suite, {
      noremap = true,
      silent = true,
      buffer = args.buf,
      desc = 'Watch Jest Test Suite',
    })
  end,
})
