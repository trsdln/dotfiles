local plugins_path = vim.fn.stdpath("data") .. "/lazy"
local lazypath = plugins_path .. "/lazy.nvim"
local pkg_manager_plugin = "folke/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/" .. pkg_manager_plugin .. ".git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

vim.api.nvim_create_user_command('OpenPluginsDir', function()
  vim.cmd.tabnew()
  vim.cmd.lcd(plugins_path)
  vim.cmd([[Dirvish .]])
end, { force = true })

vim.api.nvim_create_user_command('Dotfiles', function()
  vim.cmd.tabnew()
  vim.cmd.lcd('~/.dotfiles')
  vim.cmd([[Dirvish .]])
end, { force = true })
