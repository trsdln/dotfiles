local function define_import_commands()
  vim.cmd [[
    command! -buffer JSImportMembersSort g=\v^import\s?\{$=.+1,/\v^\}\s?from/ sort u | noh
    command! -buffer -range JSImportLevelUp <line1>,<line2>s=\V'..\/='= | noh
    command! -buffer -range JSImportLevelDown <line1>,<line2>s=\V'..\/='..\/..\/= | noh

    command! -buffer LintDiff Dispatch -compiler=eslint $PROJECT_LINTER_SCRIPT origin/main
    command! -buffer -nargs=? Lint compiler eslint | execute 'Make ' . <q-args>

    command! -buffer -nargs=? Jest compiler jest | execute 'Make -o ' . <q-args>
    command! -buffer JestDiff compiler jest | Make --changedSince=origin/main

    " Map 'c' to Ramda's compose at vim-surround
    let b:surround_99 = "R.compose(\r)"
]]
end

local function monorepo_import_bodge()
  vim.cmd [[normal! mz]]
  vim.cmd [[%s=\v'((\.\./)+packages)/([a-z\-]+/src/.+)'='@poly/\3'=]]
  vim.cmd [[%!eslint_d --stdin --stdin-filename % --fix-to-stdout]]
  vim.lsp.buf.format { async = false }
  vim.cmd [[normal! `z]]
  vim.cmd [[delmarks z]]
end


vim.api.nvim_create_autocmd('FileType', {
  pattern = { 'javascript', 'typescript' },
  group = vim.api.nvim_create_augroup('JSImportHelpers', { clear = true }),
  callback = function(args)
    define_import_commands()

    vim.keymap.set('n', '<leader>ib', monorepo_import_bodge, {
      noremap = true,
      silent = true,
      buffer = args.buf,
      desc = 'Bodge Monorepo Package Import',
    })
  end,
})
