;; Based on https://github.com/nvim-treesitter/nvim-treesitter/blob/master/queries/ecma/injections.scm

(call_expression
  function: (identifier) @_name
  (#eq? @_name "sqlF")
  arguments: ((template_string) @injection.content
                                (#offset! @injection.content 0 1 0 -1)
                                (#set! injection.include-children)
                                (#set! injection.language "sql")))

(call_expression
  function: (identifier) @_name
  (#eq? @_name "sqlObj")
  arguments: ((template_string) @injection.content
                                (#offset! @injection.content 0 1 0 -1)
                                (#set! injection.include-children)
                                (#set! injection.language "sql")))
