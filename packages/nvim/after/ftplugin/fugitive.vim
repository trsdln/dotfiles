nnoremap <buffer> cc :Git commit --no-verify<cr>
nnoremap <buffer> ca :Git commit --no-verify --amend<cr>
nnoremap <buffer> ce :Git commit --no-verify --amend --reuse-message=HEAD<cr>
nnoremap <buffer> cvc :tab Git commit --no-verify --verbose<cr>

" Open at vsplit
nnoremap <buffer> a :call <SID>FugitiveOpenCurrentFileVSplit()<cr>

function! s:FugitiveOpenCurrentFileVSplit()
  let target = s:FugitiveStatusGetCurrentFile()
  if len(target) > 0
    " Create vertical split
    vsplit
    " Move to far right
    wincmd L
    " Open target file
    execute 'edit ' . target

    " Go back to status window
    execute "normal! \<C-W>p"
    " Restore status window size
    wincmd J

    " Go back to opened file
    execute "normal! \<C-W>p"
  else
    echo "No active file to open"
  endif
endfunction

function! s:FugitiveStatusGetCurrentFile()
  let curr_line = getline('.')
  let tokens = split(curr_line, ' ')
  if len(tokens) == 2
    let file_name = tokens[1]
    return filereadable(file_name) ? file_name : ''
  else
    return ''
  endif
endfunction


augroup fugitive_configs
  autocmd!

  " Auto-cleanup hidden buffers
  autocmd BufReadPost fugitive://* set bufhidden=delete
augroup END
