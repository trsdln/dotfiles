vim.keymap.set('n', '<Leader>sr', function()
  vim.cmd.luafile(vim.fn.expand('%'))
end, {
  buffer = true,
  desc = 'Source Lua file'
})
