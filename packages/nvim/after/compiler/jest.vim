let current_compiler = "jest"

CompilerSet makeprg=CI=true\ ./node_modules/.bin/jest\ --reporters=\"${HOME}/.config/nvim/after/compiler/JestUnixReporter.js\"\ $*

CompilerSet errorformat=%f:%l:%c\ %m
