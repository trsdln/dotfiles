const safeSplit = (token, target) => typeof target === 'string' && target.split(token) || null;

const safeIndex = (index, arr) => arr instanceof Array && arr[index] || null;

const extractPositionFromMessage = (filePath, message) => {
  const messageTail = safeIndex(1, safeSplit(filePath, message));
  const position = safeIndex(0, safeSplit(')', messageTail));
  return position || '0:0';
};

const createTestCaseResultLine = filePath => testCaseResult => `.${filePath.replace(process.cwd(), '')}${extractPositionFromMessage(
  filePath,
  testCaseResult.failureMessages[0],
)} ${testCaseResult.fullName}`;

const unnest = xs => xs.reduce((acc, x) => acc.concat(x), []);

function extractUnixFriendlyTestResults(testResults) {
  const resultsLines = testResults.map(testFileResult => testFileResult.testResults
    .filter(result => result.status === 'failed')
    .map(createTestCaseResultLine(testFileResult.testFilePath)),
  );
  const flatResultsLines = unnest(resultsLines);
  return flatResultsLines.join('\n');
}

class JestUnixReporter {
  constructor(globalConfig, options) {
    this._globalConfig = globalConfig;
    this._options = options;
  }

  onRunComplete(contexts, results) {
    const unixFriendlyResultsStr = extractUnixFriendlyTestResults(
      results.testResults,
    );
    console.log(unixFriendlyResultsStr);
  }
}

module.exports = JestUnixReporter;
