" Note: use <C-^> to change keymap at insert mode
set keymap=ukrainian-custom
" Default - latin layout
set iminsert=0
" Default - latin layout in search mode
set imsearch=0
