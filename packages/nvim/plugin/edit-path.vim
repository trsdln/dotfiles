" Adds ability to edit file from path under cursor
" Basically this is improved version of vim's native `gf` binding
noremap gf :<c-u>EditFileFromCurrentPath ''<cr>

command! -nargs=? EditFileFromCurrentPath call s:EditFileFromCurrentPath(<args>)

function! s:SimplifyAndConcatPaths(path_head, path_tail)
  return simplify(a:path_head . '/' . a:path_tail)
endfunction

function! s:OpenFile(file_path, open_prefix)
  if filereadable(a:file_path)
    execute a:open_prefix
    execute "edit " . a:file_path
    return 1
  endif
  return 0
endfunction

function! s:StripJsExtension(path)
  return substitute(a:path, '\v.js$', '', '')
endfunction

function! s:EditFileFromCurrentPath(open_prefix)
  let position_str = matchstr(getline('.'), '\v\|\d+ col \d+\|')

  let raw_file_path = expand('<cfile>')
  let relative_to_cwd_path = s:SimplifyAndConcatPaths(getcwd(), raw_file_path)
  let relative_to_file_path = s:SimplifyAndConcatPaths(expand('%:p:h'), raw_file_path)

  " handle madness of JS/TS module system:
  let is_opened =
        \ s:OpenFile(relative_to_file_path, a:open_prefix) ||
        \ s:OpenFile(relative_to_file_path . '.js', a:open_prefix) ||
        \ s:OpenFile(relative_to_file_path . '/index.js', a:open_prefix) ||
        \ s:OpenFile(relative_to_file_path . '.ts', a:open_prefix) ||
        \ s:OpenFile(relative_to_file_path . '/index.ts', a:open_prefix) ||
        \ s:OpenFile(s:StripJsExtension(relative_to_file_path) . '.ts', a:open_prefix) ||
        \ s:OpenFile(relative_to_file_path . '.json', a:open_prefix) ||
        \ s:OpenFile(raw_file_path, a:open_prefix) ||
        \ s:OpenFile(relative_to_cwd_path, a:open_prefix)

  if is_opened != 1
    echom "File '" . raw_file_path . "' not found!"
    return
  endif

  " Go to specific position at file if specified (used at quicklist)
  if position_str !=# ''
    let line = matchstr(position_str, '\v\|\zs\d+\ze')
    let column = matchstr(position_str, '\vcol \zs\d+\ze\|')
    execute 'normal! ' . line . 'G' . column . '|'
  endif
endfunction
