" mnemonic: browse
nnoremap <Leader>br :call <sid>OpenAnything()<cr>

let s:url_regexp = '\vhttps?:\/\/[^ )]+'

function! s:OpenAnything()
  let current_word = expand('<cWORD>')
  let current_line = getline('.')
  let current_url = shellescape(matchstr(current_word, s:url_regexp))

  " current_url will have 2 characters if url wasn't found
  if strlen(current_url) > 2
    call g:XDGOpen(current_url)
  elseif expand('%:t') ==# 'package.json'
    " probably intention was to open NPM package web page
    let package_name = matchstr(current_line, '\v"\zs.+\ze"\:')
    call s:OpenNPMPackageURL(package_name)
  elseif &filetype ==# 'lua' && match(current_word, '\v^[''"][^''"]+/[^''"]+[''"],?$') > -1
    let token = s:ParseJSWord(current_word)
    if s:TryToOpenLocalFile(token)
      call g:XDGOpen('https://github.com/' . token)
    endif
  elseif &filetype ==# 'javascript' || &filetype ==# 'typescript' || &filetype ==# 'typescriptreact'
    let token = s:ParseJSWord(current_word)
    if s:TryToOpenLocalFile(token)
      call s:OpenNPMPackageURL(token)
    endif
  elseif s:TryToOpenLocalFile(current_word)
    call g:XDGOpen(current_word)
  endif
endfunction

" returns False if file exists
function! s:TryToOpenLocalFile(file_name)
  if filereadable(a:file_name)
    call g:XDGOpen(a:file_name)
    return 0
  endif

  let relative_path = expand('%:p:h') . '/' . a:file_name
  if filereadable(relative_path)
    call g:XDGOpen(relative_path)
    return 0
  endif

  return 1
endfunction

function! s:ParseJSWord(word)
  let tokens = split(a:word, '\v[''";,]')
  let non_empty_tkns = filter(tokens, 'v:val !=# ""')
  let middle_idx = float2nr(len(non_empty_tkns) / 2)
  if middle_idx >= 0
    return non_empty_tkns[middle_idx]
  endif
  return non_empty_tkns[0]
endfunction

function! s:OpenNPMPackageURL(package_name)
  call g:XDGOpen('https://npmjs.com/package/' . a:package_name)
endfunction

function! g:XDGOpen(url)
  let open_app_name = has('macunix') ? 'open' : 'xdg-open'
  let open_cmd = open_app_name . ' ' . a:url . ' &'
  call system(open_cmd)
  echo open_cmd
endfunction
