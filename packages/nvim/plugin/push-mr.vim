function! s:GitPushMR()
  echo "Pushing..."
  let l:out = system('git push --no-verify -u origin $(git rev-parse --abbrev-ref HEAD)')
  let l:mr_url = matchstr(l:out, '\vhttps://gitlab.com/[^ \n]+')
  if l:mr_url == ''
    echo 'No URL found'
  else
    call g:XDGOpen(l:mr_url)
  endif
endfunction

function! s:GitNewWorkBranch(branch_name)
  execute 'Git checkout -b ' . a:branch_name
  call s:GitPushMR()
endfunction

command! -nargs=1 GitNewWorkBranch call s:GitNewWorkBranch(<f-args>)
