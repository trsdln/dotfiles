function! s:ToggleTestCaseFocus()
  try
    " Go to "it" above
    silent execute 'normal! ?\v(\s|^)(it)(\.only){0,1}\(?g' . "\<cr>"

    " Figure out whether we should add or remove ".only"
    if getline('.') =~# '\v(it)\('
      silent substitute/\v(\s|^)\zs(it)\ze\(/it.only/
    else
      silent substitute/\v(\s|^)\zs(it\.only)\ze\(/it/
    endif

    nohlsearch
  catch
    echoerr "No JS test case found!"
  endtry
endfunction

function! s:ConfigureJestPlugin()
  " Navigating between test cases
  nnoremap <buffer> ]t /\v(\s\|^)\zs(it(\.(only\|skip))?\|beforeEach\|afterEach\|beforeAll\|afterAll)\(/g<cr>:noh<cr>
  nnoremap <buffer> [t ?\v(\s\|^)\zs(it(\.(only\|skip)){0,1}\|beforeEach\|afterEach\|beforeAll\|afterAll)\(?g<cr>:noh<cr>

  " Toggle test case focus
  nnoremap <buffer> <leader>jo :<c-u>ToggleTestCaseFocus<cr>

  command! ToggleTestCaseFocus call s:ToggleTestCaseFocus()
endfunction

augroup jest_configs
  autocmd!
  autocmd FileType javascript,typescript call s:ConfigureJestPlugin()
augroup END
