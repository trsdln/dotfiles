#!/usr/bin/env sh

export EDITOR='nvim'
export VISUAL='nvim'
export PAGER='less'

alias vi='nvim'
alias vim='nvim'

export GOPATH="${HOME}/.local/go"
export GOMODCACHE="${XDG_CACHE_HOME}/go/mod"
export PATH="${PATH}:${HOME}/.local/go/bin"
export JAVA_HOME="/opt/homebrew/opt/openjdk"
