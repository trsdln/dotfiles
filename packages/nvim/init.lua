require 'bootstrap'

vim.g.mapleader = ' '
vim.o.scrolloff = 8
vim.o.splitbelow = true
vim.o.splitright = true

-- case sensitivity for command line completion
vim.o.ignorecase = true
vim.o.smartcase = true

vim.o.updatetime = 250
vim.o.timeoutlen = 300

vim.o.list = true
vim.o.listchars = 'tab:» ,trail:·,nbsp:␣';

vim.o.inccommand = 'split'

vim.api.nvim_create_autocmd("TextYankPost", {
  group = vim.api.nvim_create_augroup("HighlightOnYankGroup", { clear = true }),
  pattern = { '*' },
  callback = function()
    vim.highlight.on_yank({ timeout = 300 })
  end,
})

vim.filetype.add({
  filename = {
    ['.swcrc'] = 'json',
  },
})

require 'cursor-line'
require 'restore-pos'
require 'common-maps'
require 'jest'
require 'js-import'

require("lazy").setup("plugins", {
  change_detection = { enabled = false },
})
