#!/usr/bin/env sh

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_link ".config/nvim" "."
}

dpm_package_deps() {
  dpm_define_dpm_dep "ripgrep"
  dpm_define_dpm_dep "fzf"
  dpm_define_dpm_dep "lazygit"

  dpm_define_brew_dep "neovim"
  dpm_define_brew_dep "openjdk"

  # LSPs
  dpm_define_brew_dep "lua-language-server"
  dpm_define_brew_dep "go"
  dpm_define_brew_dep "gopls"
  dpm_define_brew_dep "zls"
  dpm_define_brew_dep "rustup-init"
  # TODO: rustup component add cargo rust-src rust-std rustc rustfmt clippy rust-analyzer
  # Note: rustup doesn't create symlink for rust-analyzer after install,
  # so it should be created manually:
  # ln -s /opt/homebrew/bin/rustup-init ~/.local/cargo/bin/rust-analyzer

  # TODO: npm install -g eslint_d @fsouza/prettierd
  # npm install -g typescript typescript-language-server
 
  dpm_define_pacman_dep "neovim"
  dpm_define_pacman_dep "rust-analyzer"
  dpm_define_pacman_dep "lua-language-server"
  dpm_define_pacman_dep "lazygit"
  dpm_define_pacman_dep "python-pynvim"

  dpm_pass_error_exit || return
}
