#!/usr/bin/env sh

dpm_package_plugins() {
  dpm_define_plugin_dep "dpm-config-name"
}

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_link ".config/alacritty/alacritty.toml" \
    "$(dpm_platform_config_name).toml"
  dpm_define_link ".config/alacritty/common.toml" "common.toml"
}

dpm_package_deps() {
  dpm_define_brew_dep "font-hack"

  dpm_define_brew_dep "alacritty"

  dpm_define_pacman_dep "ttf-hack"
  dpm_define_pacman_dep "alacritty"
}
