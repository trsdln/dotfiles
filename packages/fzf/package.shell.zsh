#!/usr/bin/env zsh

if [ $(command -v fzf) ]; then
  source <(fzf --zsh)
fi
