#!/usr/bin/env sh

dpm_package_deps() {
  dpm_define_brew_dep "fzf"
  dpm_define_pacman_dep "fzf"
  dpm_define_pkg_dep "fzf"
}
