#!/usr/bin/env sh

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_script "yarn.completions.zsh"
}

dpm_package_deps() {
  dpm_define_pacman_dep "zsh"
  dpm_define_pacman_dep "zoxide"

  dpm_define_brew_dep "zoxide"

  dpm_pass_error_exit || return
  local zsh_plugins_path="$HOME/.local/zsh-plugins"
  dpm_define_git_dep \
    "https://github.com/zdharma-continuum/fast-syntax-highlighting.git" \
    "${zsh_plugins_path}/fast-syntax-highlighting"
  dpm_define_git_dep \
    "https://github.com/zsh-users/zsh-autosuggestions.git" \
    "${zsh_plugins_path}/zsh-autosuggestions"
}
