#!/usr/bin/env zsh

export KEYTIMEOUT=1 # Reduce delay for vi mode at zsh
export SHELL='/bin/zsh'
export GPG_TTY=$(tty)
export LESSHISTFILE='-' # no history file for less

# Easy re-sourcing of config
alias reload='source ~/.zshrc'

if [[ "$OSTYPE" == "darwin"* ]]; then
  alias ls='ls -G'
else
  alias ls='ls --color=auto -h'
fi

# preview syntax highlighting theme
export BAT_THEME="Solarized (dark)"

ZSH_PLUGINS_PATH="$HOME/.local/zsh-plugins"
source "${ZSH_PLUGINS_PATH}/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh"
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=blue'
# C-N to accept suggestion
bindkey '^N' autosuggest-accept

source "${ZSH_PLUGINS_PATH}/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh"

source "yarn.completions.zsh"

unset ZSH_PLUGINS_PATH

eval "$(zoxide init zsh)"
