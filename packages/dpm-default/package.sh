#!/usr/bin/env sh

dpm_package_deps() {
  # self update of dpm
  dpm_define_git_dep "git@gitlab.com:dotpm/dpm.git" "$(cd $PACKAGE_PATH/.. && pwd)/dpm"
}

