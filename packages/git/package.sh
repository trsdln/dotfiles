dpm_package_links() {
  dpm_define_link ".config/git" "."
}

dpm_package_deps() {
  dpm_define_brew_dep "git"
}
