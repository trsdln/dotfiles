#!/usr/bin/env sh

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_deps() {
  dpm_define_git_dep "git@github.com:tj/n.git" ".config/n"
}
