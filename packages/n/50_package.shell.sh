#!/usr/bin/env sh

export N_PREFIX=$HOME/.cache/n
export PATH=$HOME/.config/n/bin:$N_PREFIX/bin:$PATH
