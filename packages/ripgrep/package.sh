#!/usr/bin/env sh

dpm_package_links() {
  dpm_define_link ".config/ripgreprc" "ripgreprc"
}

dpm_package_deps() {
  dpm_define_brew_dep "ripgrep"
  dpm_define_pacman_dep "ripgrep"
  dpm_define_pkg_dep "ripgrep"
}
