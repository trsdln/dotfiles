#!/usr/bin/env sh

export TMUX_CONFIG="$HOME/.config/tmux/tmux.conf"

alias tmux="tmux -f ${TMUX_CONFIG}"
