#!/usr/bin/env sh

dpm_package_plugins() {
  dpm_define_plugin_dep "dpm-config-name"
}

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_link ".config/tmux/tmux.conf" \
    "$(dpm_platform_config_name).conf"
  dpm_define_link ".config/tmux/common.conf" "common.conf"
}

dpm_package_deps() {
  dpm_define_brew_dep "tmux"
  dpm_define_pacman_dep "tmux"
}
