## Tmux

* `C-b <space>`  - change layout
* `C-b ~`        - show previous messages from Tmux
* `C-b {`        - Swap the current pane with the previous pane
* `C-b }`        - Swap the current pane with the next pane
* `C-b C-o`      - Move panes around
* `C-b !`        - Extract current pane into new window
* `C-b .`        - Move current window
* `C-b E`        - Spread panes out evenly
* `C-b ?`        - List key bindings

### Text Selection

* `C-b [`    - selection/view mode

Then in selection mode:

* `<space>`  - start/reset selection
* `C-v`      - toggle between block/line selection modes

Note: most of Vim's motions work as well obviously.
