#!/usr/bin/env sh

dpm_package_compatible() {
  dpm_compatible "linux" || dpm_compatible "darwin"
}

dpm_package_links() {
  dpm_define_link ".config/lf" "."
}

dpm_package_deps() {
  dpm_define_dpm_dep "utils"

  dpm_define_brew_dep "lf"
  dpm_define_brew_dep "catimg"
  dpm_define_brew_dep "bat"

  dpm_define_pacman_dep "lf"
  dpm_define_pacman_dep "bat"
}
