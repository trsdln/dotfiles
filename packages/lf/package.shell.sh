#!/usr/bin/env sh

# Setup lf
# ---------
# https://github.com/gokcehan/lf/blob/master/etc/lfcd.sh
lfcd() {
  tmp="$(mktemp)"
  lf -last-dir-path="$tmp" "$@"
  if [ -f "$tmp" ]; then
    dir="$(cat "$tmp")"
    rm -f "$tmp"
    if [ -d "$dir" ]; then
      if [ "$dir" != "$(pwd)" ]; then
        cd "$dir"
      fi
    fi
  fi
}
if [ -n "$ZSH_VERSION" ]; then
  bindkey -s '^o' 'lfcd\n'
fi
