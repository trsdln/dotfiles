#!/bin/sh

preview_by_mimetype() {
  case $(file --dereference --brief --mime-type -- "$1") in
    text/* | */xml | application/json)
      bat --style plain --terminal-width "$4" -f "$1"
      ;;
    *)
      exit 1
      ;;
  esac
}

case "$1" in
  *.tgz|*.tar.gz) tar tzf "$1";;
  *.tar.bz2|*.tbz2) tar tjf "$1";;
  *.tar.txz|*.txz) xz --list "$1";;
  *.tar) tar tf "$1";;
  *.zip|*.jar|*.war|*.ear|*.oxt) unzip -l "$1";;
  *.rar) unrar l "$1";;
  *.7z) 7z l "$1";;
  *.bmp|*.jpg|*.jpeg|*.png|*.xpm) catimg -w "$4" "$1" ;;
  *)
    preview_by_mimetype $@
    ;;
esac
